##################################################
Unabashed, another Python bash interaction library
##################################################


This library can be used to launch a bash process and interact with
it; it was originally designed to retrieve variables from bash from
Python.


Status
######

Contributions are welcome ; I had this code lying around and decided
to free it, because it could be useful to more than just me, but not
everything is implemented.

