#!/usr/bin/env python
# SPDX-FileCopyrightText: 2016,2024 Jérôme Carretero <cJ-unabashed@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import subprocess
import re
import shlex
import logging
import itertools
import collections
import typing as t


logger = logging.getLogger(__name__)


def quote_string(value: str):
	"""
	"""

	if '\x00' in value:
		raise ValueError(value)

	if not value:
		return shlex.quote(value)

	a = value
	b = shlex.quote(value)

	trans = {
	 "\\": r"\\",
	 "\a": r"\a",
	 "\b": r"\b",
	 "\x1B": r"\E",
	 "\f": r"\f",
	 "\n": r"\n",
	 "\r": r"\r",
	 "\t": r"\t",
	 "\v": r"\v",
	}

	c = shlex.quote(a.translate(trans))

	#logger.debug("A=%s B=%s C=%s", a, b, c)

	if a == b == c:
		return a
	elif b == c:
		return b
	else:
		return '$' + c


def parse_string(value: collections.deque) -> str:
	"""
	Parse a bash string.

	They can look like::

	  abc
	  "abc def"
	  $'abc\tdef'
	  $"abc" (unsupported)

	"""
	out = b""

	c = value.popleft()
	if c == b"\"":
		while True:
			c = value.popleft()
			if c == b"\"":
				break
			elif c == b"\\":
				c = value.popleft()
				if c == b"x":
					out += chr(sum([ int(value.popleft(),16) * (16**i) for i in range(1,-1,-1) ])).encode()
				elif c == b"\"":
					out += b"\""
				elif c == b"$":
					out += b"$"
				elif c == b"\\":
					out += b"\\"
				elif c == b"`":
					out += b"`"
				else:
					raise NotImplementedError(c)
			else:
				out += c
	elif c == b"$":
		c = value.popleft()
		if c == b"\"": # $" translation!
			raise NotImplementedError(value)
		elif c == b"'": # $'
			while True:
				c = value.popleft()
				if c == b"'":
					break
				elif c == b"\\":
					c = value.popleft()
					if c == b"a":
						out += b"\a"
					elif c == b"b":
						out += b"\b"
					elif c == b"E":
						out += b"\x1B"
					elif c == b"f":
						out += b"\f"
					elif c == b"n":
						out += b"\n"
					elif c == b"r":
						out += b"\r"
					elif c == b"t":
						out += b"\t"
					elif c == b"v":
						out += b"\v"
					elif c == b"$":
						out += b"$"
					elif c == b"'":
						out += b"'"
					elif c == b"\"":
						out += b"\""
					elif c == b"\\":
						out += b"\\"
					elif c == b"`":
						out += b"`"
					elif c == b"?":
						out += b"?"
					elif c == b"x":
						out += bytes([sum([ int(value.popleft(),16) * (16**i) for i in range(1,-1,-1) ])])
					elif c == b"u":
						out += chr(sum([ int(value.popleft(),16) * (16**i) for i in range(3,-1,-1) ])).encode()
					elif c == b"U":
						out += chr(sum([ int(value.popleft(),16) * (16**i) for i in range(7,-1,-1) ])).encode()
					elif c in b"01234567":
						value.appendleft(c)
						out += bytes([sum([ int(value.popleft(),8) * (8**i) for i in range(2,-1,-1) ])])
					else:
						raise NotImplementedError(c)
				else:
					out += c
	else: # plain string
		out += c
		while True:
			c = value.popleft()
			if c == b"]":
				# Special case for key end
				value.appendleft(c)
				break
			out += c

	return out.decode("utf-8", errors="surrogateescape")


class Bash:
	"""
	Bash wrapper
	"""
	def __init__(self, cmd=None):
		self.process = None
		if cmd is None:
			cmd = ["bash"]
		self.cmd = cmd

	def __enter__(self):
		self.process = subprocess.Popen(self.cmd,
		 stdin=subprocess.PIPE,
		 stdout=subprocess.PIPE,
		)
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		if self.process:
			self.process.terminate()
			self.process = None

	def send(self, cmd: str):
		"""
		Send a line to the interpreter
		"""
		cmd = cmd.encode()
		logger.debug("> %s", cmd)
		self.process.stdin.write(cmd + b"\n")
		self.process.stdin.flush()

	def readline(self):
		"""
		"""
		res = self.process.stdout.readline()
		logger.debug("< %s", res.rstrip())
		return res


	def get(self, name: str, default: t.Any=None) -> t.Optional[t.Any]:
		"""
		Query the value of a specific environment variable
		"""
		if not self.process:
			return None
		command = f"declare -p {name}"
		self.send(command)
		res = self.readline().rstrip()

		logger.debug("res=%s", res)

		if (m:= re.match(rb"declare (?P<opts>\S+) (?P<key>[^=]+)=(?P<value>.*)$", res)) is None:
			logger.info("No match: %s", res)
			return

		opts = m.group("opts").decode()
		value = m.group("value")

		if opts in ("-x", "--", "-r"):
			value = collections.deque(bytes([_]) for _ in value)
			return parse_string(value)
		elif opts in ("-i", "-ir"):
			value = value.decode()
			if value.startswith("\""):
				return int(shlex.split(value)[0])
			else:
				raise NotImplementedError(res)
		elif opts.lower() in ("-a", "-ar", "-ax"):
			if "A" in opts:
				res = dict()
			else:
				res = list()

			#assert value[0] == b"(" and value[-1] == b")"
			value = collections.deque(bytes([_]) for _ in value[1:-1])

			for i in itertools.count():
				try:
					c = value.popleft()
				except IndexError:
					break
				assert c == b"[", f"Wanted beg key got {type(c)}/{c}"
				logger.debug("Parse key (cur %s)", value)
				k = parse_string(value)
				logger.debug("Key: %s", k)
				c = value.popleft()
				assert c == b"]", f"Wanted end key got {c}"
				c = value.popleft()
				assert c == b"=", f"Wanted equal got {c}"
				logger.debug("Parse value (cur %s)", value)
				v = parse_string(value)
				logger.debug("Value: %s", v)

				if "a" in opts:
					assert k == str(i)
					res.append(v)
				else:
					res[k] = v

				try:
					c = value.popleft()
				except IndexError:
					break
				if c == b" ":
					pass
				else:
					value.appendleft(c)

			return res
		else:
			raise NotImplementedError(res)

	getvariable = get

	def set(self, k, v):
		"""
		"""
		if isinstance(v, str):
			self.send("declare -- {}={}".format(k, quote_string(v)))
		elif isinstance(v, int):
			self.send(f"declare -i {k}={v}")
		elif isinstance(v, t.Mapping):
			s = []
			for _k, _v in v.items():
				s += [ "[{}]={}".format(quote_string(_k), quote_string(_v)) ]
			self.send("declare -A {}=({})".format(k, " ".join(s)))
		elif isinstance(v, t.Sequence):
			s = []
			for i, _v in enumerate(v):
				s += [ "[{}]={}".format(i, quote_string(_v)) ]
			self.send("declare -a {}=({})".format(k, " ".join(s)))
		else:
			raise NotImplementedError(f"{type(v)}/{v}")
			self.send("declare -- {}={}".format(k, quote_string(v)))

	def __getitem__(self, k):
		return self.get(k)

	def __setitem__(self, k, v):
		return self.set(k, v)

	def __delitem__(self, k):
		self.send(f"unset {k}")

	def list_variables(self) -> t.List[str]:
		"""
		List all variable names
		"""
		if not self.process:
			return []
		self.send("compgen -v; echo")
		variables = []
		while True:
			line = self.readline()
			if line == b'\n':  # Assuming an empty line as termination (might need adjustment)
				break
			variables.append(line.strip().decode())
		return variables

	def getvariables(self) -> t.Mapping[str, t.Any]:
		"""
		Get all variables and their values
		"""
		variables = {var: self.get(var) for var in self.list_variables()}
		return variables

