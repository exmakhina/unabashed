#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-unabashed@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import os
import logging
import shlex

import pytest
import hypothesis
from hypothesis import strategies as st
from hypothesis import given
from hypothesis import settings

from .bash import *


logger = logging.getLogger(__name__)


@pytest.fixture
def bash():
	with Bash() as bash:
		yield bash


@given(x=st.text(max_size=8).filter(lambda x: not "\x00" in x))
@settings(
 max_examples=10000,
 suppress_health_check=[hypothesis.HealthCheck.function_scoped_fixture],
)
def test_hypot_str(bash, x):
	#logger.info("Try %s", x)
	bash["MYSTR"] = x
	mes = bash["MYSTR"]
	assert mes == x
	del bash["MYSTR"]


@given(x=st.integers(
 min_value=-(1<<63),
 max_value=(1<<63)-1,
))
@settings(
  #max_examples=10000,
  suppress_health_check=[hypothesis.HealthCheck.function_scoped_fixture],
 )
def test_hypot_int(bash, x):
	logger.info("Try %s", x)
	bash["MYSTR"] = x
	mes = bash["MYSTR"]
	assert mes == x
	del bash["MYSTR"]


@given(x=st.dictionaries(
 keys=st.text(max_size=8).filter(lambda x: (not "\x00" in x) and x),
 values=st.text(max_size=8).filter(lambda x: not "\x00" in x),
))
@settings(
 max_examples=10000,
 suppress_health_check=[hypothesis.HealthCheck.function_scoped_fixture],
)
def test_hypot_dict(bash, x):
	#logger.info("Try %s", x)
	bash["MYSTR"] = x
	mes = bash["MYSTR"]
	assert mes == x
	del bash["MYSTR"]


@given(x=st.lists(
 elements=st.text(max_size=8).filter(lambda x: not "\x00" in x),
))
@settings(
 max_examples=10000,
 suppress_health_check=[hypothesis.HealthCheck.function_scoped_fixture],
)
def test_hypot_list(bash, x):
	#logger.info("Try %s", x)
	bash["MYSTR"] = x
	mes = bash["MYSTR"]
	assert mes == x
	del bash["MYSTR"]
