#!/usr/bin/env python
# SPDX-FileCopyrightText: 2016,2024 Jérôme Carretero <cJ-unabashed@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import os
import logging

from .bash import *


logger = logging.getLogger(__name__)


def test_101():
	with Bash() as bash:
		ref = "value"
		bash.set("MYVAR", ref)
		mes = bash.get("MYVAR")
		assert mes == ref

def test_envar():
	with Bash() as bash:
		path = bash.get("PATH").split(os.pathsep)
		logger.info("path: %s", path)

def test_getvariable_a_simple():
	with Bash() as bash:
		bash.send("declare -a ARRAY=([0]=1 [1]=2)")
		res = bash.getvariable("ARRAY")
		assert res == ["1","2"]

def test_getvariable_A_simple():
	with Bash() as bash:
		bash.send("declare -A ARRAY=([A]=1 [B]=2)")
		res = bash.getvariable("ARRAY")
		assert res == {"A": "1", "B":"2"}


def test_getvariable_aA_fancy():
	with Bash() as bash:
		bash.send("declare -A ARRAY=([$'a\t\r\nA']=1 [\"coin coin\"]=2)")
		res = bash.getvariable("ARRAY")
		assert res == {"a\t\r\nA": "1", "coin coin":"2"}


def test_dict():
	with Bash() as bash:
		bash["MYINT"] = 2
		assert bash["MYINT"] == 2
		del bash["MYINT"]

		ref = "hello\r\n\t\x1B[31;1mDangerous\x1B[0mworld"
		refs = ref
		bash["MYSTR"] = ref
		mes = bash["MYSTR"]
		assert mes == ref
		del bash["MYSTR"]

		ref = {"a": "b", "c d": refs}
		bash["MYDICT"] = ref
		mes = bash["MYDICT"]
		assert mes == ref
		del bash["MYDICT"]

		ref = ["1", "2", "3 4", refs]
		bash["MYSEQ"] = ref
		mes = bash["MYSEQ"]
		assert mes == ref
		del bash["MYSEQ"]


def test_dict_escapes():
	with Bash() as bash:
		ref = "❤️💀😭🔥🫶🐰✨✅😂"
		bash["MYSTR"] = ref
		mes = bash["MYSTR"]
		assert mes == ref
		del bash["MYSTR"]

		ref = "\x01"
		bash["MYSTR"] = ref
		mes = bash["MYSTR"]
		assert mes == ref
		del bash["MYSTR"]

		ref = "\u1234"
		bash["MYSTR"] = ref
		mes = bash["MYSTR"]
		assert mes == ref
		del bash["MYSTR"]

		ref = "\U00051234"
		bash["MYSTR"] = ref
		mes = bash["MYSTR"]
		assert mes == ref
		del bash["MYSTR"]


def test_allvars():
	with Bash() as bash:
		res = bash.getvariables()
		logger.info("Variables: %s", res)

